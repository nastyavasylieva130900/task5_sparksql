package com.epam.nastya;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;

public class AwardCounter {
    public static void main(String[] args) throws Exception {

        SparkSession spark = SparkSession
                .builder()
                .master("local")
                .appName("AwardCounter")
                .getOrCreate();

        Dataset<Row> players = spark.read().csv("src/main/resources/Master.csv")
                .select("_c0","_c3","_c4")
                .withColumnRenamed("_c0", "player_id")
                .withColumnRenamed("_c3", "first_name")
                .withColumnRenamed("_c4", "last_name");

        players = players.select(players.col("player_id"),
                functions.concat(players.col("first_name"), functions.lit(" "),
                        players.col("last_name")).as("full_name"));
        players.show();

        Dataset<Row> awards = spark.read().csv("src/main/resources/AwardsPlayers.csv")
                .select("_c0")
                .withColumnRenamed("_c0", "player_id")
                .withColumn("award_count", functions.lit(1));

        Dataset<Row> countedAwards = awards
                .groupBy("player_id")
                .sum("award_count")
                .withColumnRenamed("sum(award_count)", "award_count");


        Dataset<Row> joinedDatasets = countedAwards.join(players,
                countedAwards.col("player_id").equalTo(players.col("player_id")),
                "left")
                .drop(players.col("player_id"));

        joinedDatasets.show();

        joinedDatasets.coalesce(1).write().csv("src/main/resources/output.csv");

        spark.stop();

    }
}
